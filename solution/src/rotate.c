#include "../includes/rotate.h"

enum rotate_status rotate(struct image *from, struct image *to) {
    *to = create_image(from->height, from->width);

    if (to->pixels == NULL) {
    	return ROTATE_ERROR;
    }

    for (size_t i = 0; i < from->height; i++) {
        for (size_t j = 0; j < from->width; j++) {
          to->pixels[from->height * j + from->height - i - 1] = from->pixels[from->width * i + j];
        }
    }

    return ROTATE_OK;
}
