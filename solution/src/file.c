#include <stdio.h>

#include "../includes/file.h"

enum file_open_status open_file(FILE** file, const char* name, const char* mode) {
	*file = fopen(name, mode);

	if (*file == NULL) {
		return OPEN_ERROR;
	} else {
		return OPEN_OK;
	}
}

enum file_close_status close_file(FILE** file) {
	if (fclose(*file)) {
		return CLOSE_OK;
	} else {
		return CLOSE_ERROR;
	}
}
