#include "../includes/bmp.h"

static enum read_status get_bmp_header(FILE* file, struct bmp_header* header) {
	if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
		return READ_INVALID_HEADER;
	} else {
		return READ_OK;
	}
}

uint8_t get_padding(uint32_t width) {
    return 4 - (width * sizeof(struct pixel)) % 4;
}

enum read_status get_pixels( FILE* file, struct image *img ) {
    uint8_t padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->pixels + (i * img->width), sizeof(struct pixel), img->width, file) != img->width) {
			return READ_INVALID_SIGNATURE;
		}

		if (fseek(file, padding, SEEK_CUR) != 0) {
			return READ_INVALID_SIGNATURE;
		}
    }

    return READ_OK;
}

struct bmp_header create_header(const struct image* img) {
	uint32_t image_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;

	return (struct bmp_header) {
				.bfType = HEADER_TYPE,
            	.bfileSize = HEADER_SIZE + image_size,
            	.bfReserved = HEADER_RESERVED,
            	.bOffBits = HEADER_SIZE,
            	.biSize = HEADER_INFO,
            	.biWidth = (uint32_t)img->width,
            	.biHeight = (uint32_t)img->height,
            	.biPlanes = HEADER_PLANES,
            	.biBitCount = HEADER_BITCOUNT,
            	.biCompression = HEADER_COMPRESSION,
            	.biSizeImage = image_size,
            	.biXPelsPerMeter = HEADER_XMETR,
            	.biYPelsPerMeter = HEADER_YMETR,
            	.biClrUsed = HEADER_NCOLORS,
            	.biClrImportant = HEADER_NIMPORTANT
	};
}

enum write_status write_bmp(FILE* file, const struct image* img) {
	for (size_t i = 0; i < img->height; i++) {
		if (fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, file) != img->width){
			return WRITE_ERROR;
		}

		if (fseek(file, get_padding(img->width), SEEK_CUR) != 0) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};

    if (get_bmp_header(in, &header) != READ_OK) {
        return READ_INVALID_HEADER;
    }

    *img = create_image(header.biWidth, header.biHeight);

	if (!img->pixels)
		return READ_INVALID_SIGNATURE;
		
    enum read_status status = get_pixels(in, img);

    if (status != READ_OK) {
        free_image(img);

        return status;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
	struct bmp_header header = create_header(img);

	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}

	return write_bmp(out, img);
}
