#include <stdio.h>
#include <sysexits.h>

#include "../includes/file.h"
#include "../includes/bmp.h"
#include "../includes/image.h"
#include "../includes/rotate.h"


int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "Incorrect input format\n");
        
		return EX_DATAERR;
	}

	FILE* in = NULL;

	if (open_file(&in, argv[1], "rb") != OPEN_OK) {
        fprintf(stderr, "Incorrect name of input file\n");

        return EX_NOINPUT;
    }

    fprintf(stdout, "Input file is open\n");

    FILE* out = NULL;

	if (open_file(&out, argv[2], "wb") != OPEN_OK) {
        fprintf(stderr, "Incorrect name of output file\n");
        close_file(&in);

        return EX_CANTCREAT;
    }

    fprintf(stdout, "Output file is open\n");

    struct image from = {0};

    if (from_bmp(in, &from) != READ_OK) {
        fprintf(stderr, "Errot with read picture\n");
        close_file(&in);
        close_file(&out);

        return EX_IOERR;
    }

    fprintf(stdout, "Read picture success\n");

    struct image to = {0};

    if (rotate(&from, &to) != ROTATE_OK) {
        fprintf(stderr, "Errot with rotate picture\n");
        free_image(&from);
        close_file(&in);
        close_file(&out);

        return EX_UNAVAILABLE;
    }

    fprintf(stdout, "Rotate picture success\n");

    if (to_bmp(out, &to) != WRITE_OK) {
        fprintf(stderr, "Errot with write picture\n");
        free_image(&from);
        free_image(&to);
        close_file(&in);
        close_file(&out);

        return EX_IOERR;
    }

    fprintf(stdout, "Save picture success\n");

    free_image(&from);
    free_image(&to);
    close_file(&in);
    close_file(&out);

	return EX_OK;
}

