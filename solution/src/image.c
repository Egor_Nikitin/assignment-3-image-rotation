#include <malloc.h>

#include "../includes/image.h"

struct image create_image(size_t width, size_t height) {
	struct pixel* pixels = (struct pixel*)malloc(width * height * sizeof(struct pixel));
    struct image img = {0};

	img.height = height;
	img.width = width;
	img.pixels = pixels;

	return img;
}

void free_image(const struct image *img) {
	free(img->pixels);
}

struct pixel *get_pixel(uint32_t x, uint32_t y, struct image const *img) {
    return img->pixels + x + y * img->width;
}
