#pragma once

#include <stdbool.h>
#include <stdio.h>

enum file_open_status {
  OPEN_OK = 0,
  OPEN_ERROR,
};

enum file_close_status {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

enum file_open_status open_file(FILE** file, const char* name, const char* mode);
enum file_close_status close_file(FILE** file);
