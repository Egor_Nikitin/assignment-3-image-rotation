#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* pixels;
};

void free_image(const struct image *img);
struct image create_image(size_t width, size_t height);
