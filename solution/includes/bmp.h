#pragma once

#include  <stdint.h>

#include "../includes/image.h"

enum {
  HEADER_TYPE = 0x4D42,
  HEADER_RESERVED = 0,
  HEADER_SIZE = 54,
  HEADER_INFO = 40,
  HEADER_PLANES = 1,
  HEADER_BITCOUNT = 24,
  HEADER_COMPRESSION = 0,
  HEADER_XMETR = 0,
  HEADER_YMETR = 0,
  HEADER_NCOLORS = 0,
  HEADER_NIMPORTANT = 0
};

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );
