#pragma once

#include "./image.h"

enum rotate_status  {
  ROTATE_OK = 0,
  ROTATE_ERROR
};

enum rotate_status rotate(struct image *from, struct image *to);
